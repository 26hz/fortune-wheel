# Fortune Wheel Game

## Description

The game where we can win cash and prizes determined by spinning a giant wheel.

## How to run the game

1. clone the app to your local

2. npm install

3. npm start

## How to play
1. Click the spin button

2. Wait until the wheel stop

3. See your prize/rewards on the center of the wheel