 
// Import Application class that is the main part of our PIXI project
import { Application } from '@pixi/app'

// In order that PIXI could render things we need to register appropriate plugins
import { Renderer } from '@pixi/core' // Renderer is the class that is going to register plugins

import { BatchRenderer } from '@pixi/core' // BatchRenderer is the "plugin" for drawing sprites
Renderer.registerPlugin('batch', BatchRenderer)

import { TickerPlugin } from '@pixi/ticker' // TickerPlugin is the plugin for running an update loop (it's for the application class)
Application.registerPlugin(TickerPlugin)

// And just for convenience let's register Loader plugin in order to use it right from Application instance like app.loader.add(..) etc.
import { AppLoaderPlugin } from '@pixi/loaders'
Application.registerPlugin(AppLoaderPlugin)

// Sprite is our image on the stage
import { Sprite } from '@pixi/sprite'

var options = ["5% off", "So Close!!", "10% off", "No prize for you", "20% off", "No luck today","Free Shipping", "You can't win 'em all", "15% off", "You've got bad luck, pal"];

var startAngle = 0;
var arc = Math.PI / (options.length / 2);
var spinTimeout = null;

var spinTime = 0;
var spinTimeTotal = 0;
var spinAngleStart;

var ctx;

//call spin function when the spin button is clicked
var el = document.getElementById("spin");
if(el){
	el.addEventListener("click", spin);
}

//draw Roullette wheel function
function drawRouletteWheel() {
	//get canvas
	var canvas = document.getElementById("canvas");
	// when canvas exist
  if (canvas){
		//returns a drawing context on the canvas, or null if the context identifier is not supported.
		if (canvas.getContext) {
			var outsideRadius = 250;
			var textRadius = 180;
			var insideRadius = 100;

			// leading to the creation of a CanvasRenderingContext2D object representing a two-dimensional rendering context.
			ctx = canvas.getContext("2d");
			//clear the canvas every frame
			ctx.clearRect(0,0,500,500);
			//fonts set
			ctx.font = 'bold 12px Helvetica, Arial';

			//create the slots with the options
			for(var i = 0; i < options.length; i++) {
				var angle = startAngle + i * arc;
				//even options colored burlywood
				if(i%2 == 0){
					ctx.fillStyle = "#f8b195";
				} else{
					//odd options colored lightcoral
					ctx.fillStyle = "#f67280";
				}

				//start path selected patch
				ctx.beginPath();
				//creates an two arcs/curves (used to create circles, or parts of circles).
				ctx.arc(300, 300, outsideRadius, angle, angle + arc, false)
				ctx.arc(300, 300, insideRadius, angle + arc, angle, true);
				//draws the path you have defined
				ctx.stroke();
				ctx.fill();

				ctx.save();
				//the shadow starts 1 pixels to the left
				ctx.shadowOffsetX = -1;
				//the shadow starts 1 pixels from the bottom
				ctx.shadowOffsetY = -1;
				//set the shadow to not blur
				ctx.shadowBlur    = 0;
				//set shadow color
				ctx.shadowColor   = "rgb(220,220,220)";
				//set border
				ctx.fillStyle = "black";
				//Translates the matrix on the x and y
				ctx.translate(300 + Math.cos(angle + arc / 2) * textRadius, 
											300 + Math.sin(angle + arc / 2) * textRadius);
				//Applies a rotation transformation to the matrix
				ctx.rotate(20 * Math.PI / 180);
				//set option value
				var text = options[i];
				//add text to the slot
				ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
				//restores the most recently saved canvas state by popping the top entry in the drawing state stack
				ctx.restore();
			} 

			//Arrow
			//set arrow color
			ctx.fillStyle = "black";
			//start path selected patch
			ctx.beginPath();
			//create the arrow
			ctx.moveTo(300 - 4, 300 - (outsideRadius + 5));
			ctx.lineTo(300 + 4, 300 - (outsideRadius + 5));
			ctx.lineTo(300 + 4, 300 - (outsideRadius - 5));
			ctx.lineTo(300 + 9, 300 - (outsideRadius - 5));
			ctx.lineTo(300 + 0, 300 - (outsideRadius - 13));
			ctx.lineTo(300 - 9, 300 - (outsideRadius - 5));
			ctx.lineTo(300 - 4, 300 - (outsideRadius - 5));
			ctx.lineTo(300 - 4, 300 - (outsideRadius + 5));
			ctx.fill();
		}
	}
}

function spin() {
	//set angle start
	spinAngleStart = Math.random() * 10 + 10;
	//set the time to 0
	spinTime = 0;
	//total max spin
  spinTimeTotal = Math.random() * 3 + 4 * 1000;
  rotateWheel();
}

//rotate the wheel
function rotateWheel() {
	//add spin time
	spinTime += 30;
	//stop when it's reached to the total spin time
  if(spinTime >= spinTimeTotal) {
    stopRotateWheel();
    return;
	}
	//redraw the fortune wheel
  var spinAngle = spinAngleStart - easeOut(spinTime, 0, spinAngleStart, spinTimeTotal);
  startAngle += (spinAngle * Math.PI / 180);
  drawRouletteWheel();
  spinTimeout = setTimeout(()=>{ rotateWheel() }, 30);
}

function stopRotateWheel() {
	// clears the timeout which has been set by setTimeout()function
  clearTimeout(spinTimeout);
  var degrees = startAngle * 180 / Math.PI + 90;
  var arcd = arc * 180 / Math.PI;
  var index = Math.floor((360 - degrees % 360) / arcd);
	ctx.save();
	//the selected slot font
	ctx.font = 'bold 20px Helvetica, Arial';
	//display the selected slot
  var text = options[index]
	ctx.fillText(text, 300 - ctx.measureText(text).width / 2, 300 + 10);
	//restores the most recently saved canvas state by popping the top entry in the drawing state stack
  ctx.restore();
}

function easeOut(t, b, c, d) {
  var ts = (t/=d)*t;
  var tc = ts*t;
  return b+c*(tc + -3*ts + 3*t);
}

drawRouletteWheel();